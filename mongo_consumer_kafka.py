import sys

from pymongo import MongoClient
from kafka import KafkaConsumer
from json import loads

def consume_mongo():
    topic_name = 'TesteTwitter'
    client = MongoClient('10.142.0.5:27017')
    collection = client.test.TwiiterDemoData

    consumer = KafkaConsumer(
        topic_name,
        bootstrap_servers = ['skydemo-kafka1:9092', 'skydemo-kafka2:9092', 'skydemo-kafka3:9092'],
        auto_offset_reset='earliest',
        enable_auto_commit=True,
        value_deserializer=lambda x: loads(x.decode('utf-8')),
        group_id='mongodb_consumer_test')

    try:
        for message in consumer:
            collection.insert_one(loads(message.value.decode('utf-8')))
            print('Offset {} added to {}'.format(message.offset, collection))

    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    consume_mongo()