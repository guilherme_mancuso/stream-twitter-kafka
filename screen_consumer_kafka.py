import sys

from kafka import KafkaConsumer
from json import loads

def consume_onscreen():
    topic_name = 'TesteTwitter'

    consumer = KafkaConsumer(
        topic_name,
        bootstrap_servers = ['skydemo-kafka1:9092', 'skydemo-kafka2:9092', 'skydemo-kafka3:9092'],
        auto_offset_reset='earliest',
        enable_auto_commit=True,
        group_id='screen_exhibit')

    try:
        for message in consumer:
            message_consume = loads(message.value.decode('utf-8'))
            print(message_consume)
            print(message.topic)
            print(message.offset)

    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    consume_onscreen()