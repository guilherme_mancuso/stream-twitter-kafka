from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from kafka import KafkaProducer
from json import dumps, loads
 
CONSUMER_KEY = "arjWtg5zBwp9YRng0C9gRO38V"
CONSUMER_SECRET = "4J5FueopbhBJY1ceJmR8gml7ZIa0DtxcZ0xIZXnkwZtIIF1Mv5"
ACCESS_TOKEN = "17391574-XsOqXsN5X01DHHO99mnu33nFpY3LLdpppKvspCq5L"
ACCESS_TOKEN_SECRET = "wObeogKUFkZcNzs4bkoV8jikKskLxH0UsjBSm4kpAF9YO"

producer = KafkaProducer(
    bootstrap_servers = ['skydemo-kafka1:9092', 'skydemo-kafka2:9092', 'skydemo-kafka3:9092'],
    acks = 'all')

topic_name = 'TesteTwitter'

class TwitterStreamerToKafka():
    def stream_tweets(self, hash_tag_list):
        listener = StdOutListener()
        auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        stream = Stream(auth, listener)
        stream.filter(track=hash_tag_list)

class StdOutListener(StreamListener):
    def on_data(self, data):
        try:
            producer.send(topic_name, dumps(data).encode('utf-8'))
            tweet_data = loads(data)
            print('Sent -> id: {} - user: {}'.format(tweet_data["id"], tweet_data["user"]["screen_name"]))
            return True
        except BaseException as e:
            print("Error on_data %s" % str(e))
        return True
        
    def on_error(self, status):
        print(status)
        return False
 
if __name__ == '__main__':
    hash_tag_list = ["busca aqui"]
    twitter_streamer = TwitterStreamerToKafka()
    twitter_streamer.stream_tweets(hash_tag_list)